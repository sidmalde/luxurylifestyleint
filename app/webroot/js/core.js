var $homepageBannerAnimation = false;
var $homepageBannerTimer = false;


function homepageBanner(){
	if($homepageBannerAnimation == false){
		clearTimeout($homepageBannerTimer);
		$homepageBannerAnimation = true;
		if($('#banners > li:first').fadeOut(2000).next().fadeIn(2000).end().appendTo('#banners')){
			$('#bannerButtons li a').removeClass('on');
			$newIndex = $('#banners > li:first img').data('index')-1;
			$('#bannerButtons li:eq('+$newIndex+') a').addClass('on');
			$homepageBannerAnimation = false;
			$homepageBannerTimer = setTimeout("homepageBanner()", 5000);
		}
	}
}

$(document).ready(function () { 
	$('#slider').nivoSlider({
		effect: 'fold',
		animSpeed: 800,
		pauseTime: 2800,
	});
	
    $('#main-banner').carousel({
        interval: 3500
    }).carousel('cycle');
	
	if ($("#banners").length > 0) {
		$('#bannerButtons').width($('#bannerButtons > li').length * 25);
		//$('#bannerButtons').css('margin-left', '-'+$('#bannerButtons').width()/2+'px').fadeIn();
		$("#banners > li:gt(0)").hide();
		
		$homepageBannerTimer = setTimeout("homepageBanner()", 5000);
		$('#bannerButtons li').click(function(){
			if($homepageBannerAnimation == false && $('a', $(this)).not('.on')){
				$index = $(this).index()+1;
				if($('#banners > li:first a').data('index') != $index){
					clearTimeout($homepageBannerTimer);
					$homepageBannerAnimation = true;
					if($('#banners > li:eq('+($index-1)+')').fadeOut(2000).next().fadeIn(2000).end().appendTo('#banners')){
						$('#bannerButtons li a').removeClass('on');
						$newIndex = $('#banners > lieq('+($index-1)+') img').data('index')-1;
						$('#bannerButtons li:eq('+$newIndex+') a').addClass('on');
						$homepageBannerAnimation = false;
						$homepageBannerTimer = setTimeout("homepageBanner()", 5000);
					}
				}
			}
		});
	}
	
	
	if($('#bottom-shade .alert:not(#cookies-alert)').length > 0){
		animBottomMargin();
		$('#bottom-shade .alert:not(#cookies-alert)').addClass('timed-from-start');
		setTimeout(function() {
			$('#bottom-shade .alert.timed-from-start').remove();
			animBottomMargin();
		}, 7500);
	}
	$('#bottom-shade').on('click', '.alert .close', function(e){
		var closingElem = $(e.currentTarget).closest('.alert');
		// the folllowing line is different than animBottomMargin();
		$('#container').animate({paddingBottom: $('#bottom-shade').outerHeight()-closingElem.outerHeight() + parseInt($('#bottom-shade').css('marginBottom').replace('px', '')) +'px'}, 500);
	});
	
	if($( "ul.main-nav" ).length > 0){
		
		// mouse events
		$('.main-nav > li.custom').mouseover(function(){	$('ul.main-nav').addClass('attached');});
		$('.main-nav > li:not(.custom) > a, #header').mouseover(function(){	$('ul.main-nav').removeClass('attached');});
		$('.main-nav > li.custom > a + ul').mouseleave(function(){	$('ul.main-nav').removeClass('attached');});
		$('.main-nav li.custom, .main-nav li.custom').mouseout(function(){	$('ul.main-nav').removeClass('attached');});
		
		//scrolldown fixing to the top
		var view = $(window);
		var placeholder = $( ".main-nav" );
		var placeholderInitialTop = placeholder.offset().top;
		view.bind("scroll", function(){
			var viewTop = view.scrollTop();
			if ((viewTop > placeholderInitialTop)){
				placeholder.addClass('scrolled');
				$('.go-to-top').show();
				$('#flashMessage').addClass('scrolled');
			}else{
				placeholder.removeClass('scrolled');
				$('.go-to-top').hide();
				$('#flashMessage').removeClass('scrolled');
			}
		});
	}
});

function animBottomMargin(){
	$('#container').animate({paddingBottom: $('#bottom-shade').outerHeight() + parseInt($('#bottom-shade').css('marginBottom').replace('px', '')) +'px'}, 500);
}