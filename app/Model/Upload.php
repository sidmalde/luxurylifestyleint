<?php
class Upload extends AppModel {
	
	var $name = 'Upload';
	var $displayField = 'label';
	var $actsAs = array('Containable');
	
	var $belongsTo = array(
		'Uploader' => array(
			'className' => 'users',
			'foreignKey' => 'user_id',
		),
	);
}
