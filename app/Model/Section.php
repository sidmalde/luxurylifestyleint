<?php
App::uses('AppModel', 'Model');

class Section extends AppModel {
	var $name = 'Section';
	var $displayField = 'title';
	var $actsAs = array('Containable');
	var $order = 'created ASC';
	// Relations
	var $hasMany = array('SectionArticle');
	
	var $validate = array(
		'title' => array(
			'rule' => 'notEmpty',
			'message' => 'This is a required field and cannot be left empty.'
		),
		'content' => array(
			'rule' => 'notEmpty',
			'message' => 'This is a required field and cannot be left empty.'
		)
	);
}