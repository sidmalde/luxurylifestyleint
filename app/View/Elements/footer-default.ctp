<div class="footer">
	<div class="col-sm-12">
		<p class="text-center text-muted">
			Luxury Lifestyle International is a trading name of Eshall Limited. Registered in England & Wales No. 0693780.
		</p>
		<p class="text-center text-muted">
			9 Lewis House, School Road, London, NW10 6TD, UK
		</p>
		<p class="text-center text-muted">
			<a href="callto:<?=__('+442079938865');?>" class="text-muted"><i class="icon-phone blue-text"></i> +44 (0) 207 993 8865</a> | 
			<i class="icon-print blue-text"></i> +44 (0) 208 838 3115 | 
			<a href="mailto:info@luxlint.com" class="text-muted"><i class="icon-envelope blue-text"></i> info@luxlint.com</a>
		</p>
	</div>
	<?/*<div class="col-sm-6">
		<p class="pull-right"><?=__('Social');?></p>
	</div>*/?>
</div>