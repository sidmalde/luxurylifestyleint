<div class="navbar navbar-default navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
			<a href="#" class="navbar-brand"><?=__('Luxury Lifestyle International');?></a>
			<button class="navbar-toggle" type="button" data-toggle="collapse" data-target="#navbar-main">
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
        </div>
        <div class="navbar-collapse collapse" id="navbar-main">
			<ul class="nav navbar-nav">
				<!--<li><?=$this->Html->link(__('Users'), array('controller' => 'users', 'action' => 'index'));?></li>-->
				<li class="dropdown">
					<a class="dropdown-toggle" data-toggle="dropdown" href="#" id="themes"><?=__('Users');?><span class="caret"></span></a>
					<ul class="dropdown-menu" aria-labelledby="User Management">
						<li><?=$this->Html->link(__('All Users'), array('controller' => 'users', 'action' => 'index'));?></li>
					</ul>
				</li>
				<li class="dropdown">
					<a class="dropdown-toggle" data-toggle="dropdown" href="#" id="themes"><?=__('CMS');?><span class="caret"></span></a>
					<ul class="dropdown-menu" aria-labelledby="Content Management System">
						<li><?=$this->Html->link(__('Sections'), array('controller' => 'sections', 'action' => 'index'));?></li>
						<li><?=$this->Html->link(__('Articles'), array('controller' => 'section_articles', 'action' => 'index'));?></li>
						<li><?=$this->Html->link(__('Products'), array('controller' => 'product_types', 'action' => 'index'));?></li>
						<li><?=$this->Html->link(__('Uploads'), array('controller' => 'uploads', 'action' => 'index'));?></li>
					</ul>
				</li>
				<!--<li><a href="../help/">Help</a></li>
				<li><a href="http://news.bootswatch.com">Blog</a></li>
				<li class="dropdown">
					<a class="dropdown-toggle" data-toggle="dropdown" href="#" id="download">Download <span class="caret"></span></a>
					<ul class="dropdown-menu" aria-labelledby="download">
						<li><a tabindex="-1" href="./bootstrap.min.css">bootstrap.min.css</a></li>
						<li><a tabindex="-1" href="./bootstrap.css">bootstrap.css</a></li>
						<li class="divider"></li>
						<li><a tabindex="-1" href="./variables.less">variables.less</a></li>
						<li><a tabindex="-1" href="./bootswatch.less">bootswatch.less</a></li>
					</ul>
				</li>-->
			</ul>

			<!--<ul class="nav navbar-nav navbar-right">
				<li><a href="http://builtwithbootstrap.com/" target="_blank">Built With Bootstrap</a></li>
				<li><a href="https://wrapbootstrap.com/?ref=bsw" target="_blank">WrapBootstrap</a></li>
			</ul>-->
        </div>
	</div>
</div>