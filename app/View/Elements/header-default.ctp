<div class="container">
	<div class="main-header row">
		<div class="col-md-9">
			<?=$this->Html->image('/img/logos/logo-web.png', array('url' => '/'));?>
		</div>
		<div class="col-md-3">
			<div class="text-left">
				<h3><a href="callto:<?=__('+442079938865');?>"><i class="icon-phone blue-text"></i> <?=__('+44 (0) 207 993 8865');?></a></h3>
				<h3><a href="mailto:info@luxlint.com"><i class="icon-envelope blue-text"></i> info@luxlint.com</h3>
			</div>
			<?/*<div class="">
				<?=$this->Html->link(__('Contact Us'), '#', array('class' => 'btn btn-default btn-block btn-sm'));?>
			</div>*/?>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<div class="navbar navbar-default">
				  <div class="container">
					<div class="navbar-header">
						<a href="/" class="navbar-brand"><i class="icon icon-home"></i></a>
						<button class="navbar-toggle" type="button" data-toggle="collapse" data-target="#navbar-main">
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</button>
					</div>
					<div class="navbar-collapse collapse" id="navbar-main">
						<ul class="nav navbar-nav">
							<li><a href="/luxury-hotels"><?=__('Hotels');?></a></li>
							<li><a href="/apartments"><?=__('Apartments');?></a></li>
							<li><a href="/luxury-cars"><?=__('Cars');?></a></li>
							<li><a href="/hospitality"><?=__('Hospitality');?></a></li>
							<li><a href="/security-services"><?=__('Security Services');?></a></li>
							<li><a href="/extra-services"><?=__('Extra Services');?></a></li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>