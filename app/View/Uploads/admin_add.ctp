<h3>
	<?=@$title_for_layout;?>
	<div class="btn-group pull-right">
		<?=$this->Html->link(__('Back'), array('controller' => 'uploads', 'action' => 'index'), array('class' => 'btn btn-danger btn-sm'));?>
	</div>
</h3>

<div class="row">
	<div class="col-xs-12">
		<div class="well">
			<?=$this->Form->create('Upload', array('type' => 'file'));?>
			<?=$this->Form->input('label');?>
			<?=$this->Form->input('filename');?>
			<?=$this->Form->input('image_type', array('options' => $imageTypes, 'empty' => __('Please select an option:')));?>
			<?=$this->Form->input('file', array('type' => 'file'));?>
			<?=$this->Form->button(__('Upload'), array('type' => 'submit', 'class' => 'btn btn-success'));?>
			<?=$this->Form->end();?>
		</div>
	</div>
</div>