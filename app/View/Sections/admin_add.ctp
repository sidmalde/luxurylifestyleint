<?=$this->Form->create('Section'); ?>
	
	<?=$this->Form->input('title', array('placeholder' => 'Title'));?>
	<?=$this->Form->input('content', array('type' => 'textarea', 'placeholder' => 'Content'));?>
	<?/* =$this->Form->input('publish', array('type' => 'checkbox')); */?>
   	
	<div class="btn-group">
      <button class="btn btn-success" type="submit" data-loading-text="Saving...">Save</button>
      <?=$this->Html->link('Cancel', array('action' => 'index'), array('class' => 'btn btn-danger'), 'Are you sure you want to cancel?'); ?>		</div>
	</fieldset>
<?=$this->Form->end(); ?>
