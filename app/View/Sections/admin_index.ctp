<h2>
	<?=$title_for_layout;?>
	<div class="btn-group pull-right">
		<?=$this->Html->link(__('New Section'), array('action' => 'add'), array('class' => 'btn btn-success'));?>
	</div>
</h2>
<? if (!empty($sections)): ?>
	<table class="table table-striped table-bordered table-condensed">
		<tr>
			<th><?=__('Title');?></th>
			<th><?=__('Content');?></th>
			<th><?=__('Last Updated');?></th>
			<th>&nbsp;</th>
		</tr>
		<? foreach($sections as $section): ?>
			<tr>
				<td><?=$section['Section']['title'];?></td>
				<td><?=$section['Section']['content'];?></td>
				<td><?=$this->Time->niceDate($section['Section']['modified']);?></td>
				<td>
					<div class="btn-group pull-right">
						<?=$this->Html->link(__('Edit'), array('action' => 'edit', 'section' => $section['Section']['id']), array('class' => 'btn btn-info'));?>
						<?=$this->Html->link(__('Delete'), array('action' => 'delete', 'section' => $section['Section']['id']), array('class' => 'btn btn-danger'), __('Are you sure?'));?>
					</div>
				</td>
			</tr>
		<? endforeach; ?>
	</table>
<? else: ?>
	<div class="alert alert-default">
		<p class="text-info text-center lead"><strong><?=__('There are currently no Sections.');?></strong></p>
		<p class="text-center"><?=$this->Html->link(__('Add New Section'), array('action' => 'add', 'admin' => true), array('class' => 'btn btn-primary'));?></p>
	</div>
<? endif; ?>