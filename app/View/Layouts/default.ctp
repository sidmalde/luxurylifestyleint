<!DOCTYPE html>
<html>
<head>
	<?=$this->Html->charset(); ?>
	<title>
		<?=$title_for_layout; ?>
	</title>
	<?=$this->Html->meta('icon');?>

	<?=$this->fetch('meta');?>
	<?=$this->fetch('script');?>
	
	<?=$this->Html->script('jquery-1.10.2.min'); ?>
	
	<?=$this->Html->css('font-awesome/css/font-awesome.min'); ?>
	<?=$this->Html->css('bootstrap.min'); ?>
	<?=$this->Html->css('slate_bootstrap');?>
	<?=$this->Html->css('core'); ?>
	<?=$this->Html->css('nivo'); ?>
	<?=$this->Html->css('nivo-slider/bar/bar'); ?>
	<?=$this->Html->css('nivo-slider/dark/dark'); ?>
	<?=$this->Html->css('nivo-slider/light/light'); ?>
	<?=$this->Html->css('nivo-slider/default/default'); ?>
	
	<?=$this->Html->script('bootstrap.min'); ?>
	<?=$this->Html->script('nivo'); ?>
	<?=$this->Html->script('core'); ?>
</head>
<body>
	<?=$this->element('header-default');?>
	<?=$this->element('header-banner');?>
	<div id="container" class="container">
		<div id="content" class="row">
			<div class="col-xs-12">
				<?#=$this->Session->flash(); ?>

				<div class="content-container">
					<?=$this->fetch('content'); ?>
				</div>
			</div>
		</div>
		<div id="footer" class="row">
			<div class="col-xs-12">
			</div>
		</div>
		<div class="clear">
		</div>
	</div>
	<?=$this->element('footer-default');?>
	<?=$this->element('flash_container');?>
</body>
</html>
