<h3>
	<?=@$title_for_layout;?>
	<div class="btn-group pull-right">
		<?=$this->Html->link(__('Back'), array('controller' => 'product_types', 'action' => 'index'), array('class' => 'btn btn-danger btn-sm'));?>
	</div>
</h3>
<br />
<div class="row">
	<div class="col-xs-12">
		<div class="well">
			<?=$this->Form->create('ProductType'); ?>
				<?=$this->Form->input('name');?>
				<?=$this->Form->input('description');?>
			<?=$this->Form->end(__('Submit')); ?>
		</div>
	</div>
</div>