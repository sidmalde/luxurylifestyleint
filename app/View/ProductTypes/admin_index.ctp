<h3>
	<?=@$title_for_layout;?>
	<div class="btn-group pull-right">
		<?=$this->Html->link(__('New Product Type'), array('controller' => 'product_types', 'action' => 'add'), array('class' => 'btn btn-primary btn-sm'));?>
		<?=$this->Html->link(__('New Product'), array('controller' => 'products', 'action' => 'add'), array('class' => 'btn btn-primary btn-sm'));?>
	</div>
</h3>

<div class="row">
	<div class="col-xs-12">
		<? if(!empty($productTypes)): ?>
		<table class="table table-condensed table-striped table-bordered table-hover">
			<tr>
				<th><?=__('Product Type');?></th>
				<th><?=__('Created');?></th>
				<th><?=__('Actions');?></th>
			</tr>
			<? foreach ($productTypes as $index => $productType): ?>
				<tr>
					<td><?=$productType['ProductType']['name'];?></td>
					<td><?=$this->Time->niceDate($productType['ProductType']['created']);?></td>
					<td class="actions">	
						<div class="btn-group">
						<?=$this->Html->link(__('Actions').'<span class="caret"></span>', '#', array('escape' => false, 'class' => 'btn btn-info dropdown-toggle', 'data-toggle' => 'dropdown'));?>
						<ul class="dropdown-menu">
							<li><?=$this->Html->link(__('Products'), array('controller' => 'product_types', 'action' => 'view', 'productType' => $productType['ProductType']['id']));?></li>
							<li><?=$this->Html->link(__('Edit'), array('controller' => 'product_types', 'action' => 'edit', 'productType' => $productType['ProductType']['id']));?></li>
							<li><?=$this->Html->link(__('Delete'), array('controller' => 'product_types', 'action' => 'delete', 'productType' => $productType['ProductType']['id']));?></li>
						</ul>
						</div>
					</td>
				</tr>
			<? endforeach; ?>
		</table>
		<? else: ?>
			<div class="alert alert-info">
				<p class="text-center lead"><strong><?=__('There are currently no product types.');?></strong></p>
			</div>
		<? endif; ?>
	</div>
</div>
