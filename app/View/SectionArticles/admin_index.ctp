<h2>
	<?=$title_for_layout;?>
	<div class="btn-group pull-right">
		<?=$this->Html->link(__('New Section Article'), array('action' => 'add'), array('class' => 'btn btn-success'));?>
	</div>
</h2>
<? if (!empty($sectionArticles)): ?>
	<table class="table table-striped table-bordered table-condensed">
		<tr>
			<th><?=__('Section');?></th>
			<th><?=__('Title');?></th>
			<th><?=__('Content');?></th>
			<th><?=__('Last Updated');?></th>
			<th>&nbsp;</th>
		</tr>
		<? foreach($sectionArticles as $sectionArticle): ?>
			<tr>
				<td><?=$sectionArticle['Section']['title'];?></td>
				<td><?=$sectionArticle['SectionArticle']['title'];?></td>
				<td><?=$sectionArticle['SectionArticle']['content'];?></td>
				<td><?=$this->Time->niceDate($sectionArticle['SectionArticle']['modified']);?></td>
				<td>
					<div class="btn-group pull-right">
						<?=$this->Html->link(__('Edit'), array('action' => 'edit', 'sectionArticle' => $sectionArticle['SectionArticle']['id']), array('class' => 'btn btn-info'));?>
						<?=$this->Html->link(__('Delete'), array('action' => 'delete', 'sectionArticle' => $sectionArticle['SectionArticle']['id']), array('class' => 'btn btn-danger'), __('Are you sure?'));?>
					</div>
				</td>
			</tr>
		<? endforeach; ?>
	</table>
<? else: ?>
	<div class="alert alert-default">
		<p class="text-info text-center lead"><strong><?=__('There are currently no Section Articles.');?></strong></p>
		<p class="text-center"><?=$this->Html->link(__('Add New Section Article'), array('action' => 'add', 'admin' => true), array('class' => 'btn btn-primary'));?></p>
	</div>
<? endif; ?>