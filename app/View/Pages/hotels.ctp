<div class="row">
	<div class="col-sm-4 divider left-divider">	
		<h3>
			<?=__('The Dorchester');?>
			<div class="btn-group pull-right">
				<?=$this->Html->link(__('More Info'), array('#'), array('class' => 'btn btn-info'));?>
			</div>
		</h3>
		<br />
		<img src="/img/banner/Hotels/the-dorchester-2.jpg" class="thumbnail" />
	</div>	
	<div class="col-sm-4 divider">	
		<h3>
			<?=__('Mandarin');?>
			<div class="btn-group pull-right">
				<?=$this->Html->link(__('More Info'), array('#'), array('class' => 'btn btn-info'));?>
			</div>
		</h3>
		<br />
		<img src="/img/banner/Hotels/mandarin-1.jpg" class="thumbnail" />
	</div>
	<div class="col-sm-4 divider">	
		<h3>
			<?=__('Bvlgari');?>
			<div class="btn-group pull-right">
				<?=$this->Html->link(__('More Info'), array('#'), array('class' => 'btn btn-info'));?>
			</div>
		</h3>
		<br />
		<img src="/img/banner/Hotels/bvlgari-2.jpg" class="thumbnail" />
	</div>
</div>
<br />
<div class="row">
	<div class="col-sm-4 divider left-divider">	
		<h3><?=__('Corinthia');?></h3>
		<p class="home-box">
			Corinthia
		</p>
		<div><?=$this->Html->link(__('More Info'), array('#'), array('class' => 'btn btn-info'));?></div>
	</div>
	<div class="col-sm-4 divider">	
		<h3><?=__('Landmark');?></h3>
		<p class="home-box">
			Landmark
		</p>
		<div><?=$this->Html->link(__('More Info'), array('#'), array('class' => 'btn btn-info'));?></div>
	</div>
	<div class="col-sm-4 divider">	
		<h3><?=__('45 Park Lane');?></h3>
		<p class="home-box">
			45 Park Lane
		</p>
		<div><?=$this->Html->link(__('More Info'), array('#'), array('class' => 'btn btn-info'));?></div>
	</div>
</div>