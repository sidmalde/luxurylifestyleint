<div class="row">
	<h3><?=__('Rolls Royce Phantom');?></h3><hr />
	<div class="col-sm-9">
		<p class="home-box">
			The Rolls Royce Phantom radiates style and comfort with seating capacity for up to 3 people.
			Being driven around town in this car boasts wealth and commands acceptance from everyone around.
		</p>
		<div><?=$this->Html->link(__('More Info'), array('#'), array('class' => 'btn btn-info'));?></div>
	</div>
	<div class="col-sm-3">
		<img src="/img/uploads/Cars/1.jpg" class="thumbnail" />
	</div>
</div>
<div class="row">
	<h3><?=__('Mercedes Benz S-Class');?></h3><hr />
	<div class="col-sm-9">
		<p class="home-box">
			The Mercedes Benz S-Class ....
		</p>
		<div><?=$this->Html->link(__('More Info'), array('#'), array('class' => 'btn btn-info'));?></div>
	</div>
	<div class="col-sm-3">
		<img src="/img/uploads/Cars/2.jpg" class="thumbnail" />
	</div>
</div>