<div class="row">
	<div class="col-sm-4 divider left-divider">	
		<h3>
			<?=__('Luxury & Boutique Hotels');?>
			<div class="pull-right">
				<?=$this->Html->link(__('More Info'), array('#'), array('class' => 'btn btn-default btn-sm'));?>
			</div>
		</h3>
		<p class="home-box" align="justify">
			Luxurious suites, exquisite dining experiences, galmorous bars and 
			nourishing spas are to name a few of the facilities you will enjoy 
			at one of the hotels in our <strong>Luxury & Boutique Collection</strong>.
		</p>
	</div>	
	<div class="col-sm-4 divider">	
		<h3>
			<?=__('Luxury Apartments');?>
			<div class="pull-right">
				<?=$this->Html->link(__('More Info'), array('#'), array('class' => 'btn btn-default btn-sm'));?>
			</div>
		</h3>
		<p class="home-box" align="justify">
			If you are looking for a more permanent residence, we also have a great 
			range of Luxury Apartments on offer with a plethora of facilities on-site 
			or within close range. 
		</p>
	</div>
	<div class="col-sm-4 divider">	
		<h3><?=__('Luxury Cars');?>
			<div class="pull-right">
				<?=$this->Html->link(__('More Info'), array('#'), array('class' => 'btn btn-default btn-sm'));?>
			</div>
		</h3>
		<p class="home-box" align="justify">
			Our range of chauffeur driven cars are fit for a king; only the best will suffice. 
			A team of highly trained and experienced drivers will take you from anywhere to 
			anywhere in our stylish, luxurious and clean fleet of cars, randing from the Mercedes 
			Benz S-Class to the elite Rolls Royce Phantom.
		</p>
	</div>
</div>
<br />
<div class="row">
	<div class="col-sm-4 divider left-divider">	
		<h3><?=__('Close Protection');?>
			<div class="pull-right">
				<?=$this->Html->link(__('More Info'), array('#'), array('class' => 'btn btn-default btn-sm'));?>
			</div>
		</h3>
		<p class="home-box" align="justify">
			Close Protection
		</p>
	</div>
	<div class="col-sm-4 divider">	
		<h3><?=__('Cargo');?>
			<div class="pull-right">
				<?=$this->Html->link(__('More Info'), array('#'), array('class' => 'btn btn-default btn-sm'));?>
			</div>
		</h3>
		<p class="home-box" align="justify">
			Cargo
		</p>
	</div>
	<div class="col-sm-4 divider">	
		<h3><?=__('Hospitality');?>
			<div class="pull-right">
				<?=$this->Html->link(__('More Info'), array('#'), array('class' => 'btn btn-default btn-sm'));?>
			</div>
		</h3>
		<p class="home-box" align="justify">
			Hospitality
		</p>
	</div>
</div>