<h3>
	<?=@$title_for_layout;?>
	<div class="btn-group pull-right">
		<?=$this->Html->link(__('New User'), array('controller' => 'users', 'action' => 'add'), array('class' => 'btn btn-primary btn-sm'));?>
		<?=$this->Html->link(__('New Group'), array('controller' => 'groups', 'action' => 'add'), array('class' => 'btn btn-primary btn-sm'));?>
	</div>
</h3>

<div class="row">
	<div class="col-xs-12">
		<table class="table table-condensed table-striped table-bordered table-hover">
			<tr>
				<th><?=__('Group Name');?></th>
				<th><?=__('Created');?></th>
				<th><?=__('Actions');?></th>
			</tr>
			<? foreach ($groups as $index => $group): ?>
				<tr>
					<td><?=$group['Group']['name'];?></td>
					<td><?=$this->Time->niceDate($group['Group']['created']);?></td>
					<td><?=$this->Html->link(__('Edit'), array('action' => 'edit', 'group' => $group['Group']['id']), array('class' => 'btn btn-info'));?></td>
				</tr>
			<? endforeach; ?>
		</table>
	</div>
</div>
