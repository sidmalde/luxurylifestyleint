<?php
App::uses('AppController', 'Controller');

class UploadsController extends AppController {
	
	public function beforeFilter() {
		parent::beforeFilter();
		$this->set('bodyClass', 'uploads');
		$this->Auth->allow();
		$this->layout = 'admin';
	}
	
	public function admin_index() {
		$this->Upload->contain(array());
		$uploads = $this->Upload->find('all');
		
		$title_for_layout = __('Upload Management System');
		$this->set(compact(array('title_for_layout', 'uploads')));
	}
	
	public function admin_view() {
		
	}
	
	public function admin_add() {
		if (!empty($this->request->data)) {
			debug($this->request->data);
			
			if ($result = $this->_checkAndUploadFile('img/Banners/', 'Hotels', $this->request->data['Upload']['file'])) {
				debug($result);
				// Save record
				
				Die;
			} else {
				// could not save
			}
			die;
		}
		
		$imageTypes = array(
			'bannerHotels' => 'Banner - Hotels',
			'bannerApartments' => 'Banner - Apartments',
			'bannerCars' => 'Banner - Cars',
			'bannerCProtection' => 'Banner - Close Protection',
			'bannerHospitality' => 'Banner - Hospitality',
			'bannerCargo' => 'Banner - Cargo',
		);
		$title_for_layout = __('Upload new file');
		$this->set(compact(array('title_for_layout', 'imageTypes')));
	}
	
	public function admin_edit() {
		
	}
	
	public function admin_delete() {
		
	}
	
	public function admin_move() {
		
	}
}
