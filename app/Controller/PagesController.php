<?php
App::uses('AppController', 'Controller');

class PagesController extends AppController {
	
	function beforeFilter() {
		parent::beforeFilter();
		$this->Auth->allow();
	}
	
	public function display() {
		$path = func_get_args();

		$count = count($path);
		if (!$count) {
			return $this->redirect('/');
		}
		$page = $subpage = $title_for_layout = null;

		if (!empty($path[0])) {
			$page = $path[0];
		}
		if (!empty($path[1])) {
			$subpage = $path[1];
		}
		if (!empty($path[$count - 1])) {
			$title_for_layout = Inflector::humanize($path[$count - 1]);
		}
		$this->set(compact('page', 'subpage', 'title_for_layout'));

		try {
			$this->render(implode('/', $path));
		} catch (MissingViewException $e) {
			if (Configure::read('debug')) {
				throw $e;
			}
			throw new NotFoundException();
		}
	}
	
	public function home() {
		$title_for_layout = __('Luxury Lifestyle International Homepage');
		$this->set(compact(array('title_for_layout')));
	}
	
	public function hotels() {
		$title_for_layout = __('Luxury Lifestyle International Homepage');
		$this->set(compact(array('title_for_layout')));
	}
	
	public function apartments() {
		$title_for_layout = __('Luxury Lifestyle International Homepage');
		$this->set(compact(array('title_for_layout')));
	}
	
	public function cars() {
		$title_for_layout = __('Luxury Lifestyle International Homepage');
		$this->set(compact(array('title_for_layout')));
	}
	
	public function close_protection() {
		$title_for_layout = __('Luxury Lifestyle International Homepage');
		$this->set(compact(array('title_for_layout')));
	}
	
	public function hospitality() {
		$title_for_layout = __('Luxury Lifestyle International Homepage');
		$this->set(compact(array('title_for_layout')));
	}
	
	public function cargo() {
		$title_for_layout = __('Luxury Lifestyle International Homepage');
		$this->set(compact(array('title_for_layout')));
	}
}
