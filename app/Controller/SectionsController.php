<?php
App::uses('AppController', 'Controller');

class SectionsController extends AppController {
	
	public function beforeFilter() {
		parent::beforeFilter();
		$this->set('bodyClass', 'sections');
		$this->Auth->allow();
		$this->layout = 'default';
	}
	
	public function admin_index() {
		$this->layout = 'admin';
		$sections = $this->Section->find('all');
		$title_for_layout = __('Sections');
		$this->set(compact(array('title_for_layout', 'sections')));
	}
	
	public function admin_view() {
		$this->layout = 'admin';
		if (empty($this->params['action'])) {
			$this->Session->setFlash(__('Invalid Request'), 'flash_failure');
			$this->redirect($this->referer());
		}
		
		$title_for_layout = __('Section');
		$this->set(compact(array('title_for_layout')));
	}
	
	public function admin_add() {
		$this->layout = 'admin';
		if (!empty($this->request->data)) {
			$this->request->data['Section']['publish'] = true;
			if ($this->Section->save($this->request->data)) {
				$this->Session->setFlash(__('Section Saved successfully'), 'flash_success');
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('There was a problem saving the Section'), 'flash_failure');
			}
		}
		
		$title_for_layout = __('New Section');
		$this->set(compact(array('title_for_layout')));
	}
	
	public function admin_edit() {
		$this->layout = 'admin';
		if (empty($this->params['section'])) {
			$this->Session->setFlash(__('Invalid Request'), 'flash_failure');
			$this->redirect($this->referer());
		}
		
		if (!empty($this->request->data)) {
			$this->request->data['Section']['publish'] = true;
			if ($this->Section->save($this->request->data)) {
				$this->Session->setFlash(__('Section Saved successfully'), 'flash_success');
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('There was a problem saving the Section'), 'flash_failure');
			}
		}
		
		$this->request->data = $section = $this->Section->findById($this->params['section']);
		
		$title_for_layout = __('Section');
		$this->set(compact(array('title_for_layout', 'section')));
	}
	
	public function admin_delete() {
		$this->layout = 'admin';
		if (empty($this->params['section'])) {
			$this->Session->setFlash(__('Invalid Request'), 'flash_failure');
			$this->redirect($this->referer());
		}
		$this->layout = false;
		
		if ($this->Section->delete($this->params['section'])) {
			$this->Session->setFlash(__('Section deleted'), 'flash_success');;
		} else {
			$this->Session->setFlash(__('Unable to delete Section, please try again'), 'flash_failure');
		}
		
		$this->redirect($this->referer());
	}
}