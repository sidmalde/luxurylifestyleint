<?php
App::uses('AppController', 'Controller');

class SectionArticlesController extends AppController {
	
	public function beforeFilter() {
		parent::beforeFilter();
		$this->set('bodyClass', 'SectionArticle');
		$this->Auth->allow();
		$this->layout = 'default';
	}
	
	public function admin_index() {
		$this->layout = 'admin';
		$sectionArticles = $this->SectionArticle->find('all');
		$title_for_layout = __('Section Articles');
		$this->set(compact(array('title_for_layout', 'sectionArticles')));
	}
	
	public function admin_add() {
		$this->layout = 'admin';
		if (!empty($this->request->data)) {
			$this->request->data['SectionArticle']['publish'] = true;
			if ($this->SectionArticle->save($this->request->data)) {
				$this->Session->setFlash(__('Section Article Saved successfully'), 'flash_success');
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('There was a problem saving the Section Article'), 'flash_failure');
			}
		}
		
		$sections = $this->SectionArticle->Section->find('list');
		
		$title_for_layout = __('New Section Article');
		$this->set(compact(array('title_for_layout', 'sections')));
	}
	
	public function admin_edit() {
		$this->layout = 'admin';
		if (empty($this->params['sectionArticle'])) {
			$this->Session->setFlash(__('Invalid Request'), 'flash_failure');
			$this->redirect($this->referer());
		}
		
		if (!empty($this->request->data)) {
			if ($this->SectionArticle->save($this->request->data)) {
				$this->Session->setFlash(__('Section Article Saved successfully'), 'flash_success');
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('There was a problem saving the SectionArticle'), 'flash_failure');
			}
		}
		
		$this->request->data = $sectionArticle = $this->SectionArticle->findById($this->params['sectionArticle']);
		$sections = $this->SectionArticle->Section->find('list');
		
		$title_for_layout = __('Section Article');
		$this->set(compact(array('title_for_layout', 'sectionArticle', 'sections')));
	}
	
	public function admin_delete() {
		$this->layout = 'admin';
		if (empty($this->params['sectionArticle'])) {
			$this->Session->setFlash(__('Invalid Request'), 'flash_failure');
			$this->redirect($this->referer());
		}
		$this->layout = false;
		
		if ($this->SectionArticle->delete($this->params['sectionArticle'])) {
			$this->Session->setFlash(__('Section Article deleted'), 'flash_success');;
		} else {
			$this->Session->setFlash(__('Unable to delete Section Article, please try again'), 'flash_failure');
		}
		
		$this->redirect($this->referer());
	}
}