<?php
App::uses('AppController', 'Controller');

class ProductTypesController extends AppController {
	
	public function beforeFilter() {
		parent::beforeFilter();
		$this->set('bodyClass', 'product_types');
		$this->Auth->allow();
		$this->layout = 'admin';
	}
	
	public function admin_index() {
		$productTypes = $this->ProductType->find('all');
		$title_for_layout = __('Product Type Management System');
		$this->set(compact(array('title_for_layout', 'productTypes')));
	}
	
	public function admin_view() {
		
	}
	
	public function admin_add() {
		if (!empty($this->request->data)) {
			$this->ProductType->create();
			if ($this->ProductType->save($this->request->data)) {
				$this->Session->setFlash(__('Product Type saved successfully'), 'flash_success');
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('Product Type could not be saved, please try again'), 'flash_failure');
			}
		}
		
		$title_for_layout = __('Product Type :: New');
		$this->set(compact(array('title_for_layout')));
	}
	
	public function admin_edit() {
		if (empty($this->request->params['productType'])) {
			$this->Session->setFlash(__('Invalid Request'), 'flash_failure');
			$this->redirect(array('action' => 'index'));
		}
		
		if (!empty($this->request->data)) {
			if ($this->ProductType->save($this->request->data)) {
				$this->Session->setFlash(__('Product Type updated successfully'), 'flash_success');
				$this->redirect(array('action' => 'view', 'productType' => $this->request->params['productType']));
			} else {
				$this->Session->setFlash(__('Product Type could not be updated, please try again'), 'flash_failure');
			}
		}
		
		$productType = $this->ProductType->findById($this->request->params['productType']);
		
		$title_for_layout = __('Product Type :: New');
		$this->set(compact(array('title_for_layout', 'productType')));
	}
	
	public function admin_delete() {
		
	}
}
